## PyMongo Taster Project

The goal of this project was to understand how pymongo works and how I can connect the MongoDB client to python. I was able to insert a dictionary (obtained from an API request) into a collection within a database inside MongoDB. I was also able to query the database using pymongo aggregate function. For example, to find the highest count in the collection I used:

`maxval = my_db['region_count'].aggregate([
    {
        '$group' : {'_id': '$region',
                    'max_count': {'$max':'$count'}}
    },
    {
        '$project': {'_id': 0, 'max_count':1}
    }
])
for m in maxval:
    print('max count is:', m['max_count'])`


I later used this knowledge to create an automated data pipeline using apache airflow, pymongo and python requests library. The project can be found [here](https://gitlab.com/MattySanchez20/apache-airflow-mongodb-pipeline).
